const express = require("express");
const morgan = require("morgan");
const fs = require("fs");
const path = require("path");

const folderName = path.join(__dirname, "api");
const folderName2 = path.join(__dirname, "api", "files");
const fileForlogs = path.join(__dirname, "logs.log");
try {
  if (!fs.existsSync(folderName)) {
    fs.mkdirSync(folderName);
  }
} catch (err) {
  console.error(err);
}

try {
  if (!fs.existsSync(folderName2)) {
    fs.mkdirSync(folderName2);
  }
} catch (err) {
  console.error(err);
}

try {
  if (!fs.existsSync(fileForlogs)) {
    fs.appendFileSync(fileForlogs, "", "utf8");
  }
} catch (e) {
  console.error(e);
}

const app = express();
const myrouter = require("./routes/filesRouter");
const accessLogStream = fs.createWriteStream(fileForlogs, { flags: "a" });
app.use(morgan(":method  my-tsatys:status  :url  :http-version"));
app.use(morgan("combined", { stream: accessLogStream }));
app.use(express.json());
app.use("/api/files", myrouter);

app.use(function (err, req, res, next) {
  console.error(err);
  res.status(500).json({
    message: "Internal server error in reqbodu",
  });
});

app.listen(8080, (err) => {
  if (err) {
    console.log("serve", err);
  }

  console.log("listening 8080");
});
