const express = require("express");
const { createFile } = require("../controllers/createFile");
const { deletefile } = require("../controllers/deletefile");
const { getFiles } = require("../controllers/getFiles");
const getOnefile = require("../controllers/getOnefile");
const { updatefile } = require("../controllers/updatefile");
const myrouter = express.Router();

myrouter.post("/", createFile);
myrouter.get("/", getFiles);
myrouter.get("/:filename", getOnefile);
myrouter.put("/", updatefile);
myrouter.delete("/:filename", deletefile);

module.exports = myrouter;
