const { writeFile, access } = require("fs");
const path = require("path");
const extentions = [".log", ".json", ".yaml", ".xml", ".txt", ".js"];
const pathForsTORAGE = path.join(__dirname, "..", "api", "files");
console.log(pathForsTORAGE);
const makePathWitFile = (nameoffile) => {
  try {
    return path.join(pathForsTORAGE, nameoffile);
  } catch (e) {
    console.log(e);
  }
};
console.log(makePathWitFile("fg.txt"));

const createFile = (req, res) => {
  const { filename, content } = req.body;
  console.log("fn", __dirname);

  if (!content) {
    res.status(400).send({
      message: "Please specify 'content' parameter",
    });
    return;
  }

  if (!filename) {
    res.status(400).send({
      message: "Please specify 'filename' parameter",
    });
    return;
  }

  const isType = extentions.some((el) => filename.endsWith(el));

  if (!isType) {
    res.status(400).send({
      message: "wrong type",
    });
    return;
  }
  access(makePathWitFile(filename), (err) => {
    if (!err) {
      res.status(400).send({ message: `file already ${filename} exists` });
      return;
    }

    writeFile(makePathWitFile(filename), content, (err, result) => {
      if (err) {
        console.log(err);
        res.status(500).send({ message: "Server error" });
        return;
      }
      res.status(200).send({ message: "File created successfully" });
    });
  });
};

module.exports = { createFile };
