const { readFile, stat, writeFile } = require("fs");
const path = require("path");

const updatefile = (req, res) => {
  const { filename, content } = req.body;

  if (!content) {
    res.status(400).send({
      message: "no cont par",
    });
    return;
  }

  if (!filename) {
    res.status(400).send({
      message: "no filename par",
    });
    return;
  }

  const pathtofile = path.join(__dirname, "..", "api", "files", filename);
  stat(pathtofile, (err, stat) => {
    if (err) {
      res.status(400).json({ message: "file does not exist" });
      return;
    }

    writeFile(pathtofile, content, (err, dat) => {
      if (err) {
        res.status(500).json({
          message: "Server error",
        });
        return;
      }
      res.status(200).send({
        message: "File updated successfully",
      });
    });
  });
};

module.exports = { updatefile };
