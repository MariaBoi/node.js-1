const { writeFile, readdir } = require("fs");
const path = require("path");

const pathForsTORAGE = path.join(__dirname, "..", "api", "files");
const getFiles = (req, res) => {
  try {
    readdir(pathForsTORAGE, (er, dta) => {
      if (er) {
        res.status(500).json({
          message: "Server error",
        });
        return;
      }
      res.status(200).json({
        message: "Success",
        files: dta,
      });
    });
  } catch (e) {
    console.log(e);
    res.status(400).json({
      message: "client error",
    });
  }
};

module.exports = { getFiles };
