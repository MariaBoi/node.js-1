const { readFile, stat } = require("fs");
const path = require("path");
const pathtofile = path.join(__dirname, "..", "api", "files");

const getOnefile = (req, res) => {
  const { filename } = req.params;

  stat(path.join(pathtofile, filename), (err, stats) => {
    if (err) {
      res.status(400).json({
        message: `No file with '${filename}' filename found`,
      });
      return;
    }
    readFile(path.join(pathtofile, filename), "utf-8", (err, dta) => {
      if (err) {
        res.status(500).json({
          message: "Internal server error",
        });
        return;
      }
      res.status(200).json({
        message: "Success",
        filename,
        content: dta,
        extension: filename.match(/(?<=\.)[^.]+$/i)[0],
        uploadedDate: stats.birthtime,
      });
    });
  });
};

module.exports = getOnefile;
