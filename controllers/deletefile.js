const { unlink, access } = require("fs");
const path = require("path");

const deletefile = (req, res) => {
  const { filename } = req.params;
  const filepath = path.join(__dirname, "..", "api", "files", filename);
  access(filepath, (er) => {
    if (er) {
      res.status(400).json({
        message: `No file with '${filename}' filename found`,
      });

      return;
    }
    unlink(filepath, (er) => {
      if (er) {
        res.status(500).json({
          //   message: "Server error",
          message: `No file with '${filename}' filename found`,
        });
        return;
      }
      res.status(200).json({
        message: "Success",
      });
    });
  });
};

module.exports = { deletefile };
